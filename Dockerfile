FROM python:3.6-alpine

RUN apk upgrade && apk add --no-cache libpq libjpeg-turbo zlib
COPY requirements.txt /requirements.txt

RUN apk add --no-cache --virtual build-dependencies \
                                # python/postgres
                                linux-headers \
                                gcc \
                                python3-dev \
                                musl-dev \
                                postgresql-dev \
                                # pillow
                                jpeg-dev\
                                zlib-dev \
  && rm -rf /var/cache/apk/* \
  && pip install  --no-cache-dir  -r /requirements.txt \
  && apk del build-dependencies


WORKDIR /app/
COPY . /app/
RUN chmod +x docker-entrypoint.sh
EXPOSE 3000


# Start uWSGI
#CMD ["uwsgi", "--http", ":3000", "--module", "todolist_project.wsgi"]
