#!/bin/sh

./manage.py migrate
celery -A todolist_project worker --loglevel=info --beat &
uwsgi --http :3000 --module todolist_project.wsgi