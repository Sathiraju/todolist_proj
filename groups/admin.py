from django.contrib import admin
from .models import Group, UserGroupRole

admin.site.register(Group)
admin.site.register(UserGroupRole)
