from django.contrib.auth.models import User
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

from groups.models import (
    Group,
    UserGroupRole,
)


class GroupsTest(APITestCase):

    def setUp(self):
        self.username = "tester"
        self.password = "testing"

        self.urls = {
            'token_auth': 'web_tokens:obtain-token-pair',
            'token_refresh': 'web_tokens:refresh-token',
        }

        self.user_bob = User.objects.create_user(
            username=self.username,
            password=self.password,
        )

        self.user_alice = User.objects.create_user(
            username=self.username + '2',
            password=self.password,
        )

        self.group = Group.objects.create(title='Group_1',  creator=self.user_bob)
        self.user_group_role = UserGroupRole.objects.create(
            user=self.user_bob,
            group=self.group,
            is_admin=True
        )

        self.another_admin = User.objects.create_user(
            username='admin',
            password=self.password
        )

        self.testing_data = {
            'title': 'MyGroup',
        }
        self.access_token = self._get_access_token(self.username, self.password)
        self.access_token_auth = f"{self.access_token}"

    def tearDown(self):
        pass

    def _get_access_token(self, username, password):
        auth = {"username": username, "password": password}
        url = reverse('web_tokens:obtain-token-pair')
        response = self.client.post(url, data=auth)
        self.assertIsNotNone(response.data.get('access_token'))
        access_token = response.data.get('access_token')
        return access_token

    def test_get_group_users(self):
        url = reverse('groups:groups-viewset-users-list', args=[self.group.id])
        response = self.client.get(url, HTTP_AUTHORIZATION=self.access_token)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)
        self.assertEqual(response.data[0].get('user'), self.user_bob.id)
        self.assertEqual(response.data[0].get('is_admin'), self.user_group_role.is_admin)

    def test_add_group_users(self):
        url = reverse('groups:groups-viewset-users-list', args=[self.user_bob.id])
        response = self.client.get(url, HTTP_AUTHORIZATION=self.access_token)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)
        self.assertEqual(response.data[0].get('user'), self.user_bob.id)
        self.assertEqual(response.data[0].get('is_admin'), self.user_group_role.is_admin)

    def test_create_group_ok(self):
        url = reverse('groups:groups-viewset-list')
        response = self.client.post(url, self.testing_data, HTTP_AUTHORIZATION=self.access_token)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED, response.data)
        self.assertEqual(response.data.get('title'), self.testing_data.get('title'))
        print(response.data)
        self.assertEqual(response.data.get('creator'), self.user_bob.id)
        # Check creator is Admin
        user_group_role = UserGroupRole.objects.get(user=self.user_bob, group=self.group)
        self.assertEqual(user_group_role.group, self.group)
        self.assertEqual(user_group_role.user, self.user_bob)

    def test_creator_delete_group_ok(self):
        url = reverse('groups:groups-viewset-detail', args=[self.group.id])
        response = self.client.delete(url, HTTP_AUTHORIZATION=self.access_token)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT, response.data)

    def test_admin_patch_group_ok(self):
        UserGroupRole.objects.create(group=self.group, user=self.user_alice, is_admin=True)
        url = reverse('groups:groups-viewset-detail', args=[self.group.id])
        data = {'title': 'Patched title'}
        access_token = self._get_access_token(self.user_alice.username, self.password)
        response = self.client.patch(url, data=data, HTTP_AUTHORIZATION=access_token)
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)
        self.assertEqual(response.data.get('title'), data.get('title'), response.data)

    def test_not_admin_patch_group_forbidden(self):
        UserGroupRole.objects.create(group=self.group, user=self.user_alice, is_admin=False)
        url = reverse('groups:groups-viewset-detail', args=[self.group.id])
        data = {'title': 'Patched title'}
        access_token = self._get_access_token(self.user_alice.username, self.password)
        response = self.client.patch(url, data=data, HTTP_AUTHORIZATION=access_token)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN, response.data)

    def test_creator_patch_group_ok(self):
        url = reverse('groups:groups-viewset-detail', args=[self.group.id])
        data = {'title': 'Patched title'}
        response = self.client.patch(url, data=data, HTTP_AUTHORIZATION=self.access_token)
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)
        self.assertEqual(response.data.get('title'), data.get('title'), response.data)

    def test_creator_get_group_ok(self):
        url = reverse('groups:groups-viewset-detail', args=[self.group.id])
        response = self.client.get(url, HTTP_AUTHORIZATION=self.access_token)
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)
        self.assertEqual(response.data.get('title'), self.group.title)
        print(response.data)
        self.assertEqual(response.data.get('creator'), self.user_bob.id)

    def test_not_creator_delete_group_forbidden(self):
        access_token = self._get_access_token(self.user_alice.username, self.password)
        self.access_token_auth = f"{self.access_token}"
        url = reverse('groups:groups-viewset-detail', args=[self.group.id])
        response = self.client.delete(url, HTTP_AUTHORIZATION=access_token)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN, response.data)

    def test_not_creator_patch_group_forbidden(self):
        url = reverse('groups:groups-viewset-detail', args=[self.group.id])
        data = {'title': 'Patched title'}
        access_token = self._get_access_token(self.user_alice.username, self.password)
        self.access_token_auth = f"{self.access_token}"
        response = self.client.patch(url, data=data, HTTP_AUTHORIZATION=access_token)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN, response.data)

    def test_not_creator_get_group_ok(self):
        url = reverse('groups:groups-viewset-detail', args=[self.group.id])
        access_token = self._get_access_token(self.user_alice.username, self.password)
        self.access_token_auth = f"{self.access_token}"
        response = self.client.get(url, HTTP_AUTHORIZATION=access_token)
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)

    def test_creator_delete_group_user(self):
        UserGroupRole.objects.create(
            user=self.user_alice,
            is_admin=False,
            group=self.group,
        )
        url = reverse('groups:groups-viewset-users-detail', args=[self.group.id, self.user_alice.id])
        access_token = self._get_access_token(self.user_bob, self.password)
        self.access_token_auth = f"{self.access_token}"
        response = self.client.delete(url, HTTP_AUTHORIZATION=access_token)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT, response.data)

    def test_admin_delete_group_user_ok(self):
        UserGroupRole.objects.create(
            user=self.another_admin,
            group=self.group,
            is_admin=True,
        )
        url = reverse('groups:groups-viewset-users-detail', args=[self.group.id, self.another_admin.id])
        access_token = self._get_access_token(self.another_admin, self.password)
        self.access_token_auth = f"{self.access_token}"
        response = self.client.delete(url, HTTP_AUTHORIZATION=access_token)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT, response.data)

    def test_admin_make_group_user_admin_ok(self):
        UserGroupRole.objects.create(
            user=self.another_admin,
            group=self.group,
            is_admin=True,
        )
        url = reverse('groups:groups-viewset-users-detail', args=[self.group.id, self.user_alice.id])
        access_token = self._get_access_token(self.another_admin, self.password)
        self.access_token_auth = f"{self.access_token}"
        alice_group_role = UserGroupRole.objects.create(
            user=self.user_alice,
            group=self.group,
            is_admin=False
        )
        data = {'is_admin': True}
        response = self.client.patch(url, data=data, HTTP_AUTHORIZATION=access_token)
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)
        alice_group_role = UserGroupRole.objects.get(
            user=self.user_alice,
            group=self.group
        )
        self.assertEqual(alice_group_role.is_admin, True, response.data)

    def test_get_group_users_list_ok(self):
        alice_group_role = UserGroupRole.objects.create(
            user=self.user_alice,
            group=self.group,
            is_admin=False,
        )
        url = reverse('groups:groups-viewset-users-list', args=[self.group.id])
        response = self.client.get(url, HTTP_AUTHORIZATION=self.access_token)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 2)
        self.assertEqual(response.data[0].get('user'), self.user_bob.id, response.data)
        self.assertEqual(response.data[0].get('is_admin'), self.user_group_role.is_admin, response.data)
        self.assertEqual(response.data[1].get('user'), self.user_alice.id, response.data)
        self.assertEqual(response.data[1].get('is_admin'), alice_group_role.is_admin, response.data)
