from django.db.models import Count
from django.shortcuts import get_list_or_404, get_object_or_404
from rest_framework import permissions
from rest_framework import status
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.exceptions import MethodNotAllowed
from rest_framework.response import Response

from web_tokens.authentication import JWTAuthentication
from .models import Group, UserGroupRole
from .permissions import IsAdminReadOnly
from .serializers import GroupSerializer, UsersGroupSerializer


class GroupViewset(viewsets.ModelViewSet):
    queryset = Group.objects.all()
    serializer_class = GroupSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,
                          IsAdminReadOnly)
    authentication_classes = (JWTAuthentication,)

    def perform_create(self, serializer):
        serializer.save(creator=self.request.user)

    def get_queryset(self):
        qs = super().get_queryset()
        return qs.annotate(num_users=Count('users'))

    @action(methods=['get'], url_path='users',
            detail=True, serializer_class=UsersGroupSerializer)
    def users_list(self, request, pk):

        if request.method == 'GET':
            user_group_roles = get_list_or_404(UserGroupRole, group_id=pk)
            serializer = UsersGroupSerializer(user_group_roles, many=True)
            return Response(serializer.data)

        raise MethodNotAllowed

    @action(methods=['get', 'delete', 'patch'], url_path='users/(?P<user_in_group_id>[0-9]+)',
            detail=True, serializer_class=UsersGroupSerializer)
    def users_detail(self, request, pk, user_in_group_id):
        instance = get_object_or_404(UserGroupRole, user_id=user_in_group_id, group_id=pk)

        if request.method == 'DELETE':
            instance.delete()
            return Response(status=status.HTTP_204_NO_CONTENT)

        if request.method == 'PATCH':
            serializer = self.get_serializer(instance, data=request.data)
            serializer.is_valid(raise_exception=True)
            serializer.save()
            return Response(status=status.HTTP_200_OK)

        if request.method == 'GET':
            user_group_roles = get_object_or_404(UserGroupRole, group_id=pk, user_id=user_in_group_id)
            serializer = UsersGroupSerializer(user_group_roles)
            return Response(serializer.data)

        raise MethodNotAllowed
