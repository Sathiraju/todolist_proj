
from django.contrib.auth.models import User
from rest_framework import serializers
from rest_framework.validators import UniqueValidator

from .models import Todo, TodoList


class TodoSerializer(serializers.ModelSerializer):
    user = serializers.ReadOnlyField(source='user.username')

    class Meta:
        model = Todo
        fields = ('id', 'image', 'title', 'text', 'user', 'todo_list',
                  'to_be_done_date', 'failed', 'created_at', 'is_done')


class TodoUploadImageSerializer(serializers.ModelSerializer):
    image = serializers.ImageField(required=True)

    class Meta:
        model = Todo
        fields = ('image',)


class ListOfTodoListsSerializer(serializers.ModelSerializer):
    user = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False)

    class Meta:
        model = TodoList
        fields = ('id', 'title', 'user', 'completed')


class TodoListDetailSerializer(serializers.ModelSerializer):
    todos = serializers.PrimaryKeyRelatedField(many=True, read_only=True, required=False)
    user = serializers.StringRelatedField()

    class Meta:
        model = TodoList
        fields = ('id', 'user', 'title', 'todos')


class UserSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(required=True, validators=[UniqueValidator(queryset=User.objects.all())])
    username = serializers.CharField(validators=[UniqueValidator(queryset=User.objects.all())])
    todo_lists = serializers.PrimaryKeyRelatedField(many=True, required=False, read_only=True)
    password = serializers.CharField(min_length=8, write_only=True)

    def create(self, validated_data):
        user = User.objects.create_user(
            validated_data['username'],
            validated_data['email'],
            validated_data['password']
        )
        return user

    class Meta:
        model = User
        fields = ('id', 'username', 'todo_lists', 'email', 'password')
