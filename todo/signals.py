from django.db.models.signals import post_delete
from django.dispatch import receiver

from todo.models import Todo


@receiver(post_delete,  sender=Todo)
def delete_todo(sender, instance, **kwargs):
    instance.image.delete()
