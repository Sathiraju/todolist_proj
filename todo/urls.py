from . import views

from django.conf.urls import url
from rest_framework import routers

router = routers.SimpleRouter()
router.register(r'', views.TodoViewSet, base_name="todo-viewset")


urlpatterns = [
    # Users
    url(r'^users/$', views.UserList.as_view(), name='users-list'),
    url(r'^users/(?P<pk>[0-9]+)/$', views.UserDetail.as_view(), name='user-detail'),

    # TodoLists
    # Create and View lists of TodoLists
    url(r'^lists/$', views.ListOfTodoLists.as_view(), name='todolists-list'),
    # Read, update and delete todo_list
    url(r'^lists/(?P<pk>[0-9]+)/$', views.TodoListDetail.as_view(), name='todolist-detail'),
] + router.urls
