from django.apps import AppConfig


class WebTokensConfig(AppConfig):
    name = 'web_tokens'
