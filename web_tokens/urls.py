from . import views

from django.conf.urls import url


urlpatterns = [
    url(r'^obtain-token-pair/', views.ObtainTokenPair.as_view(), name='obtain-token-pair'),
    url(r'^refresh-token/', views.RefreshTokenPair.as_view(), name='refresh-token'),
]
